package br.edu.ubiratanmourao.infolibras;

import android.content.Intent;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;

import br.edu.ubiratanmourao.infolibras.R;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void openLeitorQRCODE(View view){
        Intent intent = new Intent(this, leitorQrCode.class);
        startActivity(intent);
    }

}
