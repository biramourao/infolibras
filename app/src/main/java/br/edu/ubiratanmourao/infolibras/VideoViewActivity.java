package br.edu.ubiratanmourao.infolibras;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnPreparedListener;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

public class VideoViewActivity extends Activity {

    private VideoView myVideoView;
    private int position = 0;
    private ProgressDialog progressDialog;
    private MediaController mediaControls;
    private AlertDialog alerta;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_video_view);

        if (mediaControls == null) {
            mediaControls = new MediaController(this);
        }

        myVideoView = findViewById(R.id.videoViewComp);

        if(getIntent().hasExtra("placa")) {
            String placa = getIntent().getStringExtra("placa");
            traduzirPlaca(placa);
        }

        myVideoView.requestFocus();
        myVideoView.setOnPreparedListener(new OnPreparedListener() {

            public void onPrepared(MediaPlayer mediaPlayer) {

                myVideoView.seekTo(position);
                if (position == 0) {
                    myVideoView.start();
                } else {
                    //if we come from a resumed activity, video playback will be paused
                    myVideoView.pause();

                }
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt("Position", myVideoView.getCurrentPosition());
        myVideoView.pause();
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        //we use onRestoreInstanceState in order to play the video playback from the stored position
        position = savedInstanceState.getInt("Position");
        myVideoView.seekTo(position);
    }
    public void fecharVideo(View view){
        this.finish();
    }

    private void mensagem() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Placa não encontrada");
        builder.setMessage("Esse QRCODE não pertence ao InfoLIBRAS!");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                finish();
            }
        });
        alerta = builder.create();
        alerta.show();
    }
    public void traduzirPlaca(String pPlaca){
        try {
            myVideoView.setMediaController(mediaControls);

            //verifica qual placa foi lida e reproduz o video correspondente
            switch (pPlaca) {
                case "bloco-I":
                    myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bloco_i));
                    break;
                case "banheiro-feminino":
                    myVideoView.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.banheiro_feminino));
                    break;
                default:
                    mensagem();
                    break;

            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
    }

}