package br.edu.ubiratanmourao.infolibras

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result
import kotlinx.android.synthetic.main.activity_leitor_qr_code.*
import me.dm7.barcodescanner.zxing.ZXingScannerView
import pub.devrel.easypermissions.EasyPermissions
import pub.devrel.easypermissions.PermissionRequest
import br.edu.ubiratanmourao.infolibras.util.*


class leitorQrCode : Activity(),
        ZXingScannerView.ResultHandler,
        EasyPermissions.PermissionCallbacks {

    val REQUEST_CODE_CAMERA = 182 /* Inteiro aleatório */
    val REQUEST_CODE_FULLSCREEN = 184 /* Inteiro aleatório */
    var tv_bar_code_type = 0
    var tv_content = ""


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_leitor_qr_code)

        askCameraPermission()
    }

    override fun onResume() {
        super.onResume()
        z_xing_scanner.setResultHandler(this)
        restartCameraIfInactive()
    }

    private fun restartCameraIfInactive() {
        if (!z_xing_scanner.isCameraStarted()
                && EasyPermissions.hasPermissions(this, Manifest.permission.CAMERA)) {
            startCamera()
        }
    }

    override fun onPause() {
        super.onPause()
        z_xing_scanner.stopCameraForAllDevices()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_CODE_FULLSCREEN) {

            ib_flashlight.tag = !data!!.getBooleanExtra(Database.KEY_IS_LIGHTENED, false)
            flashLight()

            if (resultCode == Activity.RESULT_OK) {
                processBarcodeResult(
                        data.getStringExtra(Database.KEY_NAME),
                        data.getStringExtra(Database.KEY_BARCODE_NAME))
            }
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int,
            permissions: Array<String>,
            grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        /* Encaminhando resultados para EasyPermissions */
        EasyPermissions.onRequestPermissionsResult(
                requestCode,
                permissions,
                grantResults,
                this)
    }

    override fun onPermissionsDenied(
            requestCode: Int,
            perms: MutableList<String>) {

        askCameraPermission()
    }

    private fun askCameraPermission() {
        EasyPermissions.requestPermissions(
                PermissionRequest.Builder(this, REQUEST_CODE_CAMERA, Manifest.permission.CAMERA)
                        .setRationale(getString(R.string.request_permission_description))
                        .setPositiveButtonText(getString(R.string.request_permission_button_ok))
                        .setNegativeButtonText(getString(R.string.request_permission_button_cancel))
                        .build())
    }

    override fun onPermissionsGranted(
            requestCode: Int,
            perms: MutableList<String>) {

        startCamera()
    }

    private fun startCamera() {
        if (!z_xing_scanner.isFlashSupported(this)) {
            ib_flashlight.visibility = View.GONE
        }

        z_xing_scanner.startCameraForAllDevices(this)
    }

    override fun handleResult(result: Result?) {

        if (result == null) {
            unrecognizedCode(this, { reproduzirVideo() })
            return
        }

        processBarcodeResult(
                result.text,
                result.barcodeFormat.name)
    }

    private fun processBarcodeResult(text: String, barcodeFormatName: String) {

        val resultSaved = Database.getSavedResult(this)
        if (resultSaved == null || !resultSaved.text.equals(text, true)) {
            notification(this)
        }

        val result = Result(
                text,
                text.toByteArray(), /* Somente para ter algo */
                arrayOf(), /* Somente para ter algo */
                BarcodeFormat.valueOf(barcodeFormatName))

        Database.saveResult(this, result)

        tv_content = result.text
        reproduzirVideo()
    }

    fun reproduzirVideo(view: View? = null) {

        val intent = Intent(this, VideoViewActivity::class.java)
        intent.putExtra("placa",tv_content)
        startActivity(intent)
        turnOffFlashlight()
    }

    fun flashLight(view: View? = null){
        val value = if(ib_flashlight.tag == null)
            true
        else
            !(ib_flashlight.tag as Boolean)
        ib_flashlight.tag = value /* Sempre o inverso do valor de entrada. */

        if(value){
            z_xing_scanner.enableFlash(this, true)
            ib_flashlight.setImageResource(R.drawable.ic_flashlight_white_24dp)
        }
        else{
            z_xing_scanner.enableFlash(this, false)
            ib_flashlight.setImageResource(R.drawable.ic_flashlight_off_white_24dp)
        }
    }

    private fun turnOffFlashlight() {
        ib_flashlight.tag = true
        flashLight()
    }
}